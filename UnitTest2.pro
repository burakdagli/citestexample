#-------------------------------------------------
#
# Project created by QtCreator 2014-06-26T15:25:31
#
#-------------------------------------------------

QT       += widgets testlib

TARGET = tst_testprojecttest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += tst_testprojecttest.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"
