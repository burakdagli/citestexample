#include <QString>
#include <QtWidgets>
#include <QtTest>

class TestProjectTest : public QObject
{
    Q_OBJECT

public:
    TestProjectTest();

private Q_SLOTS:
    void testCase1();
    void shouldUpper();
    void shouldUPPER();
    void shouldLower();
    void shouldLow();
    void shouldLow1();
    void shouldLow2();
    void shouldLow3Ulann();

    void testGui();
};


TestProjectTest::TestProjectTest()
{
}

void TestProjectTest::testCase1()
{
    QVERIFY2(true, "Failure");
}

void TestProjectTest::shouldUpper()
{
    QString str = "Hello";
    QVERIFY(str.toUpper() == "HELL" );
}

void TestProjectTest::shouldUPPER()
{
    QString str = "Hello";
    QCOMPARE(str.toUpper(), QString("ELLO"));
}

void TestProjectTest::shouldLower()
{
    QString str = "Hello";
    QCOMPARE(str.toLower(), QString("hello"));
}

void TestProjectTest::testGui()
{
    QLineEdit lineEdit;

    lineEdit.show();



    QTest::keyClicks(&lineEdit, "hello world");




    QCOMPARE(lineEdit.text(), QString("hello world"));
}


QTEST_MAIN(TestProjectTest)

#include "tst_testprojecttest.moc"
